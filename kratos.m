% Kratos (Κράτος), spirit of strength, might, power, and sovereign rule
function kratos()
    % Read config files
    defaultConfigScriptName = 'kratosconf_default';
    userConfigScriptName = 'kratosconf';
    conf = readConfig(defaultConfigScriptName, userConfigScriptName);

    % Setup commands
    commands = getCommandDefs();
    validateCommands(commands);

    % Say hello
    if conf.doWelcome
        displayWelcomeMessage(userConfigScriptName);
    end

    while true
        str = input('>','s');

        cmd = strsplit(str);

        if strcmp(cmd{1},'stop')
            break;
        end

        parseCmd(commands, cmd, conf);
    end
end

function displayWelcomeMessage(userConfigScriptName)
    fprintf('Welcome! This is Kratos, spirit of strength, might, power, and sovereign rule\n')
    configpath = which(userConfigScriptName);

    if ~isempty(configpath)
        fprintf('Config file at %s was used.\n', configpath);
    else
        fprintf('No user config file was found. Using defaults.\n')
    end
end

function parseCmd(commands, cmd, conf)
    for i = 1:length(commands)
        if ismember(cmd{1}, commands{i}{1})
            commands{i}{3}(cmd, conf);
            return
        end
    end
    fprintf('Command not found\n')
end

function commands = getCommandDefs()
    commands = {
        {
            {'stop'},                             % Command name
            'Stop Kratos.',                       % Description
            @(cmd, conf)[],                       % Command function
        },
        {
            {'list', 'ls'},
            'List all students and their status.',
            @cmdList,
        },
        {
            {'print', 'pr'},
            'Print comments for all students.',
            @cmdPrintComments
        },
        {
            {'printtable', 'prt'},
            'Print comments for all students. Use tabs. This allow pasting on to a spreadsheet program',
            @cmdPrintCommentsForTable
        },
        {
            {'comment', 'c'},
            'Review a single student.',
            @cmdComment,
        },
        {
            {'just_comment', 'jc'},
            'Write comment for a student.',
            @cmdJustComment,
        },
        {
            {'all'},
            'Review all students in order.',
            @cmdAll,
        },
        {
            {'from'},
            'Review all students from a given id.',
            @cmdFrom,
        },
        {
            {'allmissing', 'missing', 'miss', 'am'},
            'Review all students without comments.',
            @cmdAllMissing,
        }
        {
            {'help', 'h'},
            'Display help message.',
            @cmdHelp,
        }
    };
end

% Check the the command defenitions are valid
function validateCommands(commands)
    cmdNames = {};
    for i = 1:length(commands)
        if nargin(commands{i}{3}) ~= 2
            error('The command function for ''%s'' has the wrong number of arguments', commands{i}{1}{1})
            return
        end

        cmdNames = [cmdNames commands{i}{1}];
    end

    if ~length(cmdNames) == length(unique(cmdNames))
        error('There are conflicting command names.');
    end
end



%%%%%%%%%%%%%%%%%%%%%%%
%% COMMAND FUNCTIONS %%
%%%%%%%%%%%%%%%%%%%%%%%
function cmdList(cmd, conf)
    names = getNames();
    for i = 1:length(names)
        if exist(['./' names{i} '/comment.txt'], 'file')
            c = 'x';
        else
            c = ' ';
        end

        fprintf('%2d[%s]: %s\n', i, c, names{i});
    end
end

function cmdPrintComments(cmd, conf)
    names = getNames();
    n = length(names);

    for i = 1:n
        b = getComment(names{i});
        if ~isempty(b)
            fprintf('%s:\n%s\n\n',names{i},b);
        else
            fprintf('%s:\n[]\n\n',names{i});
        end
    end
end

function cmdPrintCommentsForTable(cmd, conf)
    names = getNames();
    n = length(names);

    for i = 1:n
        b = getComment(names{i});
        if ~isempty(b)
            fprintf('%s\t%s\n',names{i},b);
        else
            fprintf('%s\t_\n',names{i});
        end
    end
end

function cmdComment(cmd, conf)
    names = getNames();
    if length(cmd) ~= 2
        fprintf('Provide an id to view and comment!\n')
        return;
    end

    I = sscanf(cmd{2},'%d');

    examineFile(names{I})
end

function cmdJustComment(cmd, conf)
    names = getNames();
    if length(cmd) ~= 2
        fprintf('Provide an id to view and comment!\n')
        return;
    end

    I = sscanf(cmd{2},'%d');

    promptComment(names{I})
end

function cmdAll(cmd, conf)
    names = getNames();
    for i = 1:length(names)
        examineFile(names{i})
    end
end

function cmdFrom(cmd, conf)
    names = getNames();
    if length(cmd) ~= 2
        fprintf('Provide an id to start from!\n')
        return;
    end

    I = sscanf(cmd{2},'%d');
    for i = I:length(names)
        examineFile(names{i})
    end
end

function cmdAllMissing(cmd, conf)
    names = getNames();
    hc = hasComments(names);
    toRun = names(~hc);
    toRun
    for i = 1:length(toRun)
        examineFile(toRun{i})
    end
end

function cmdHelp(cmd, conf)
    commands = getCommandDefs();

    fprintf('Available commands:\n');
    for i = 1:length(commands)
        names = commands{i}{1};
        descr = commands{i}{2};
        fprintf('%s   ', names{1});
        if length(names) > 1
            fprintf('(%s', names{2})
            for i = 3:length(names)
                fprintf(', %s', names{i});
            end
            fprintf(')')
        end
        fprintf('\n')
        fprintf('    %s\n', descr)
    end
end




%%%%%%%%%%%%%%%%%%%%%
%% OTHER FUNCTIONS %%
%%%%%%%%%%%%%%%%%%%%%

function o = hasComments(names)
    n = length(names);
    hc = zeros(1,n);

    for i = 1:n
        hc(i) = exist(['./' names{i} '/comment.txt'], 'file');
    end

    o = logical(hc);
end

function c = getComment(name)
    filename = ['./' name '/comment.txt'];

    if ~exist(filename, 'file')
        c = [];
        return
    end

    f = fopen(filename, 'r', 'n', 'utf-8');
    c = fread(f, inf, 'char');
    fclose(f);
end

function names = getNames()
    contents = dir('.');

    names = {};

    for item = contents'
        name = item.name;

        if strcmp(name, '.') || strcmp(name, '..')
            continue
        end

        if ~item.isdir
            continue
        end

        names{end+1} = name;
    end
end


function examineFile(name)
    files = getFiles(['./' name]);
    mfiles = dir(['./' name '/*.m']);

    % Print name and files
    fprintf('===================================\n');
    fprintf('Namn: %s\n', name);
    fprintf('===================================\n');
    fprintf('Submitted files:\n')
    for i = 1:length(files)
        fprintf('\t%s\n', files(i).name);
    end
    fprintf('\n');

    % Print files
    printable = {'.m', '.asv', '.txt'};

    for i = 1:length(files)
        [~, ~, ext] = fileparts(files(i).name);
        if ismember(ext, printable)
            fprintf('%s\n', files(i).name);
            fprintf('-----------------------------------\n');
            printFile([name '/' files(i).name])
            fprintf('-----------------------------------\n\n');
        end
    end

    % Run and comment
    if length(mfiles)>=1
        files = {mfiles.name};
        I = promptOptions('Which file to run?', files, 0);
        while I ~= 0
            fprintf('-----------------------------------\n');
            runAndCatch([name '/' mfiles(I).name]);
            fprintf('-----------------------------------\n');
            I = promptOptions('Which file to run?', files, 0);
        end
    else
        fprintf('========\nNo m-files found.\n========\n', name)
    end

    fprintf('\n\n===================================\n');

    promptComment(name)

    close all;
end

function files = getFiles(d)
    filesAndDirs = dir(['./' d]);

    I = [];
    for i = 1:length(filesAndDirs)
        if ~filesAndDirs(i).isdir
            I(end+1) = i;
        end
    end
    files = filesAndDirs(I);
end

% Prompts for a choice between options.
% The return value will be the index of the option. If none is picked return 0
% If a default value is provided it is returned if the user presses enter.
function I = promptOptions(question, options, default)
    defaultArg('default', [])

    fprintf('%s\n', question);

    for i = 1:length(options)
        fprintf('\t%d: %s', i, options{i});

        if i == default
            fprintf(' [default]');
        end

        fprintf('\n');
    end

    if isempty(default)
        while true
            try
                I = input('Option # (0 for none): ');
            catch
                fprintf('Try again!\n');
                continue
            end
            if ~isempty(I) && 0 <= I && I <= length(options)
                return
            end
            fprintf('Try again!\n');
        end
    else
        while true
            try
                I = input('Option # (0 for none, [enter] for default): ');
            catch
                fprintf('Try again!\n');
                continue
            end

            if isempty(I)
                I = default;
            end

            if 0 <= I && I <= length(options)
                return
            end

            fprintf('Try again!\n');
        end
    end
end

function printFile(fn)
    fid = fopen(fn, 'r', 'n', 'utf-8');

    n = 1;
    line = fgetl(fid);
    while ischar(line)
        fprintf('%3d |%s\n', n, line);
        n = n+1;
        line = fgetl(fid);
    end

    fclose(fid);
end

function promptComment(name)
    prevComment = getComment(name);
    if ~isempty(prevComment)
        fprintf('Previous comment: "%s"\n',prevComment);
    else
        fprintf('No previous comment.\n')
    end
    commandwindow;
    comment = input('? ', 's');
    writeComment(name, comment)
end

function writeComment(name, comment)
    if length(comment) > 0
        f = fopen([name '/comment.txt'], 'w', 'n', 'utf-8');
        fprintf(f, '%s', comment);
        fclose(f);
    end
end

function runAndCatch(filnamn)
    try
        run(filnamn)
    catch e
        fprintf(2, '\n%s', getReport(e));
    end
end

function defaultArg(s, val)
    if evalin('caller',sprintf('~exist(''%s'',''var'') || isempty(%s)',s,s))
        assignin('caller',s,val)
    end
end

% Runs a config script found on the matlab path.
% The config script should configure the conf struct according
% to the users preferences
function conf = readConfig(defaultScriptName, userScriptName)
    %The following scripts will populate and change the conf struct
    run(defaultScriptName);

    if exist(userScriptName) == 2
        run(userScriptName);
    end
end
