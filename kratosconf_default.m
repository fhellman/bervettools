% These are the default values for the kratos configuration.
% DO NOT change this file.
% Any configuration should be done in a file called 'kratosconf.m'
% on the Matlab path.
conf.doWelcome = true;
