#!/bin/bash
# Lyssa (Λύσσα), spirit of rage, fury and rabies in animals
find . -name 'comment.txt' |
    sort |
    while read a
    do
        echo ====== `dirname "$a"` =====
        cat "$a" | xclip -loops 1 -verbose 2> /dev/null
    done
